﻿namespace GmCore.Tweenings
{
	public abstract class Tweening
	{
		private TweeningFunction function;

		protected Tweening(TweeningFunction function)
		{
			this.function = function;
		}

		public float Value
		{
			get;
			protected set;
		}

		public void Update(float time, float begin, float change, float duration)
		{
			switch (this.function)
			{
				case TweeningFunction.EaseIn:
					this.Value = this.EaseIn(time, begin, change, duration);
					break;

				case TweeningFunction.EaseOut:
					this.Value = this.EaseOut(time, begin, change, duration);
					break;

				case TweeningFunction.EaseInOut:
					this.Value = this.EaseInOut(time, begin, change, duration);
					break;

				default:
					this.Value = 0f;
					break;
			}
		}

		public float Update(float time, float begin, float change, float duration, TweeningFunction function)
		{
			switch (function)
			{
				case TweeningFunction.EaseIn:
					return this.EaseIn(time, begin, change, duration);

				case TweeningFunction.EaseOut:
					return this.EaseOut(time, begin, change, duration);

				case TweeningFunction.None:
					return this.EaseInOut(time, begin, change, duration);

				default:
					return 0f;
			}
		}

		protected abstract float EaseIn(float time, float begin, float change, float duration);

		protected abstract float EaseOut(float time, float begin, float change, float duration);

		protected abstract float EaseInOut(float time, float begin, float change, float duration);
	}
}