﻿namespace GmCore.Tweenings
{
	using System;

	public class ElasticTweening : Tweening
	{
		public ElasticTweening(TweeningFunction function)
			: base(function)
		{
		}

		protected override float EaseIn(float time, float begin, float change, float duration)
		{
			if (time == 0f)
			{
				return begin;
			}

			time = time / duration;

			if (time == 1f)
			{
				return begin + change;
			}

			var p = duration * .3f;
			var s = p / 4f;
			time = time - 1f;

			return -(float) (change * Math.Pow(2f, 10f * time) * Math.Sin((time * duration - s) * (2f * Math.PI) / p) + begin);
		}

		protected override float EaseOut(float time, float begin, float change, float duration)
		{
			if (time == 0f)
			{
				return begin;
			}

			time = time / duration;

			if (time == 1f)
			{
				return begin + change;
			}

			var p = duration * .3f;
			var s = p / 4f;

			return (float) (change * Math.Pow(2f, -10 * time) * Math.Sin((time * duration - s) * (2f * Math.PI) / p) + change + begin);
		}

		protected override float EaseInOut(float time, float begin, float change, float duration)
		{
			if (time == 0f)
			{
				return begin;
			}

			time = time / duration / 2f;

			if (time == 2f)
			{
				return begin + change;
			}

			var p = duration * (.3f * 1.5f);
			var a = change;
			var s = p / 4f;

			if (time < 1f)
			{
				time = time - 1f;

				return -.5f * (float) (a * Math.Pow(2f, 10f * time) * Math.Sin((time * duration - s) * (2f * Math.PI) / p)) + begin;
			}

			time = time - 1f;

			return (float) (a * Math.Pow(2f, -10f * time) * Math.Sin((time * duration - s) * (2f * Math.PI) / p) * .5f + change + begin);
		}
	}
}