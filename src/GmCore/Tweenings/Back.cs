﻿namespace GmCore.Tweenings
{
	public class BackTweening : Tweening
	{
		public BackTweening(TweeningFunction function)
			: base(function)
		{
		}

		protected override float EaseIn(float time, float begin, float change, float duration)
		{
			time = time / duration;

			return change * time * time * ((1.70158f + 1f) * time - 1.70158f) + begin;
		}

		protected override float EaseOut(float time, float begin, float change, float duration)
		{
			time = time / duration - 1f;

			return change * (time * time * ((1.70158f + 1f) * time + 1.70158f) + 1f) + begin;
		}

		protected override float EaseInOut(float time, float begin, float change, float duration)
		{
			var s = 1.70158f;
			time = time / duration / 2f;

			if (time < 1f)
			{
				s = s * 1.525f;

				return change / 2f * (time * time * ((s + 1f) * time - s)) + begin;
			}

			time = time - 2f;
			s = s * 1.525f;

			return change / 2f * (time * time * ((s + 1f) * time + s) + 2f) + begin;
		}
	}
}