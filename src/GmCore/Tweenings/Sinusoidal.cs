﻿namespace GmCore.Tweenings
{
	using System;

	public class SinusoidalTweening : Tweening
	{
		public SinusoidalTweening(TweeningFunction function)
			: base(function)
		{
		}

		protected override float EaseIn(float time, float begin, float change, float duration)
		{
			return -change * (float) Math.Cos(time / duration * (Math.PI / 2f)) + change + begin;
		}

		protected override float EaseOut(float time, float begin, float change, float duration)
		{
			return change * (float) Math.Sin(time / duration * (Math.PI / 2f)) + begin;
		}

		protected override float EaseInOut(float time, float begin, float change, float duration)
		{
			return (-change / 2f) * ((float) Math.Cos(Math.PI * time / duration) - 1f) + begin;
		}
	}
}