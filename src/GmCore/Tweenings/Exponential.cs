﻿namespace GmCore.Tweenings
{
	using System;

	public class ExponentialTweening : Tweening
	{
		public ExponentialTweening(TweeningFunction function)
			: base(function)
		{
		}

		protected override float EaseIn(float time, float begin, float change, float duration)
		{
			return (time == 0f) ? begin : (change * (float) Math.Pow(2f, 10f * (time / duration - 1f)) + begin);
		}

		protected override float EaseOut(float time, float begin, float change, float duration)
		{
			return (time == duration) ? (begin + change) : (change * (float) (-Math.Pow(2f, -10f * time / duration) + 1f) + begin);
		}

		protected override float EaseInOut(float time, float begin, float change, float duration)
		{
			if (time == 0f)
			{
				return begin;
			}

			if (time == duration)
			{
				return begin + change;
			}

			time = time / duration / 2f;

			if (time < 1f)
			{
				return (change / 2f) * (float) Math.Pow(2f, 10f * (time - 1f)) + begin;
			}

			time = time - 1f;

			return (change / 2f) * (float) (-Math.Pow(2f, -10f * time) + 2f) + begin;
		}
	}
}