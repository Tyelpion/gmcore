﻿namespace GmCore.Tweenings
{
	public class BounceTweening : Tweening
	{
		public BounceTweening(TweeningFunction function)
			: base(function)
		{
		}

		protected override float EaseIn(float time, float begin, float change, float duration)
		{
			return change - this.EaseOut(duration - time, 0f, change, duration) + begin;
		}

		protected override float EaseOut(float time, float begin, float change, float duration)
		{
			time = time / duration;

			if (time < (1f / 2.75f))
			{
				return change * (7.5625f * time * time) + begin;
			}
			else if (time < (2f / 2.75f))
			{
				time = time - (1.5f / 2.75f);

				return change * (7.5625f * time * time + .75f) + begin;
			}
			else if (time < (2.5f / 2.75f))
			{
				time = time - (2.25f / 2.75f);

				return change * (7.5625f * time * time + .9375f) + begin;
			}
			else
			{
				time = time - (2.625f / 2.75f);

				return change * (7.5625f * time * time + .984375f) + begin;
			}
		}

		protected override float EaseInOut(float time, float begin, float change, float duration)
		{
			if (time < duration / 2f)
			{
				return this.EaseIn(time * 2f, 0f, change, duration) * 0.5f + begin;
			}
			else
			{
				return this.EaseOut(time * 2f - duration, 0f, change, duration) * .5f + change * 0.5f + begin;
			}
		}
	}
}