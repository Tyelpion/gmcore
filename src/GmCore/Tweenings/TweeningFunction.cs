﻿namespace GmCore.Tweenings
{
	public enum TweeningFunction
	{
		None, EaseIn, EaseOut, EaseInOut
	}
}