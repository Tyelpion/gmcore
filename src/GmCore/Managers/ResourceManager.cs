﻿namespace GmCore
{
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Audio;
	using Microsoft.Xna.Framework.Content;
	using Microsoft.Xna.Framework.Graphics;
	using Microsoft.Xna.Framework.Media;
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Windows.Forms;
	using System.Xml.Linq;

	public static class ResourceManager
	{
		private static int screenWidth = 800;
		private static int screenHeight = 600;
		private static Form gameWindow;
		private static Dictionary<string, Texture2D> textures = new Dictionary<string, Texture2D>();
		private static Dictionary<string, TextureRegion> textureRegions = new Dictionary<string, TextureRegion>();
		private static Dictionary<string, SoundEffect> sounds = new Dictionary<string, SoundEffect>();
		private static Dictionary<string, Song> songs = new Dictionary<string, Song>();
		private static Dictionary<string, SpriteFont> fonts = new Dictionary<string, SpriteFont>();
		public static Random Random = new Random();

		public static int ScreenWidth
		{
			get { return screenWidth; }
		}

		public static int ScreenHeight
		{
			get { return screenHeight; }
		}

		public static void SetGameWindow(GameWindow window)
		{
			gameWindow = Control.FromHandle(window.Handle) as Form;
			screenWidth = window.ClientBounds.Width;
			screenHeight = window.ClientBounds.Height;
		}

		public static bool IsGameActive()
		{
			return Form.ActiveForm == gameWindow;
		}

		public static void AddTexture(string name, Texture2D texture)
		{
			if (!textures.ContainsKey(name) && texture != null)
			{
				textures.Add(name, texture);
			}
		}

		public static void AddTextureRegion(string name, TextureRegion textureRegion)
		{
			if (!textureRegions.ContainsKey(name) && textureRegion != null)
			{
				textureRegions.Add(name, textureRegion);
			}
		}

		public static void AddSound(string name, SoundEffect sound)
		{
			if (!sounds.ContainsKey(name) && sound != null)
			{
				sounds.Add(name, sound);
			}
		}

		public static void AddSong(string name, Song song)
		{
			if (!songs.ContainsKey(name) && song != null)
			{
				songs.Add(name, song);
			}
		}

		public static void AddFont(string name, SpriteFont font)
		{
			if (!fonts.ContainsKey(name) && font != null)
			{
				fonts.Add(name, font);
			}
		}

		public static Texture2D GetTexture(string name)
		{
			if (!textures.ContainsKey(name))
				throw new KeyNotFoundException();

			return textures[name];
		}

		public static TextureRegion GetTextureRegion(string name)
		{
			if (!textureRegions.ContainsKey(name))
				throw new KeyNotFoundException();

			return textureRegions[name];
		}

		public static SoundEffect GetSound(string name)
		{
			if (!sounds.ContainsKey(name))
				throw new KeyNotFoundException();

			return sounds[name];
		}

		public static Song GetSong(string name)
		{
			if (!songs.ContainsKey(name))
				throw new KeyNotFoundException();

			return songs[name];
		}

		public static SpriteFont GetFont(string name)
		{
			if (!fonts.ContainsKey(name))
				throw new KeyNotFoundException();

			return fonts[name];
		}

		public static void RemoveTexture(string name)
		{
			if (textures.ContainsKey(name))
			{
				textures.Remove(name);
			}
		}

		public static void RemoveTextureRegion(string name)
		{
			if (textureRegions.ContainsKey(name))
			{
				textureRegions.Remove(name);
			}
		}

		public static void RemoveSound(string name)
		{
			if (sounds.ContainsKey(name))
			{
				sounds.Remove(name);
			}
		}

		public static void RemoveSong(string name)
		{
			if (songs.ContainsKey(name))
			{
				songs.Remove(name);
			}
		}

		public static void RemoveFont(string name)
		{
			if (fonts.ContainsKey(name))
			{
				fonts.Remove(name);
			}
		}

		public static void LoadResources(ContentManager contentManager, string path)
		{
			if (File.Exists(path))
			{
				var doc = XDocument.Load(path);

				LoadTextures(doc, contentManager);
				LoadSounds(doc, contentManager);
				LoadSongs(doc, contentManager);
				LoadFonts(doc, contentManager);
			}
		}

		private static bool IsValidDirection(string value)
		{
			return !string.IsNullOrEmpty(value) && (value.Equals("vertical") || value.Equals("horizontal"));
		}

		private static void LoadSounds(XDocument doc, ContentManager contentManager)
		{
			if (doc != null)
			{
				var sounds = doc.Root.Element("Sounds");
				if (sounds != null)
				{
					foreach (var sound in sounds.Elements("Sound"))
					{
						if (sound.Attribute("name") != null && sound.Attribute("source") != null)
						{
							var content = contentManager.Load<SoundEffect>(sound.Attribute("source").Value);
							AddSound(sound.Attribute("name").Value, content);
						}
					}
				}
			}
		}

		private static void LoadSongs(XDocument doc, ContentManager contentManager)
		{
			if (doc != null)
			{
				var songs = doc.Root.Element("Songs");
				if (songs != null)
				{
					foreach (var song in songs.Elements("Song"))
					{
						if (song.Attribute("name") != null && song.Attribute("source") != null)
						{
							var content = contentManager.Load<Song>(song.Attribute("source").Value);
							AddSong(song.Attribute("name").Value, content);
						}
					}
				}
			}
		}

		private static void LoadFonts(XDocument doc, ContentManager contentManager)
		{
			if (doc != null)
			{
				var fonts = doc.Root.Element("Fonts");
				if (fonts != null)
				{
					foreach (var font in fonts.Elements("Font"))
					{
						if (font.Attribute("name") != null && font.Attribute("source") != null)
						{
							var content = contentManager.Load<SpriteFont>(font.Attribute("source").Value);
							AddFont(font.Attribute("name").Value, content);
						}
					}
				}
			}
		}

		private static void LoadTextures(XDocument doc, ContentManager contentManager)
		{
			if (doc != null)
			{
				var textures = doc.Root.Element("Textures");
				if (textures != null)
				{
					foreach (var texture in textures.Elements("Texture"))
					{
						var textureSource = texture.Attribute("source");
						var textureName = texture.Attribute("name");
						if (textureSource != null &&
							textureName != null &&
							!string.IsNullOrEmpty(textureSource.Value) &&
							!string.IsNullOrEmpty(textureName.Value))
						{
							var content = contentManager.Load<Texture2D>(textureSource.Value);
							AddTexture(textureName.Value, content);

							if (texture.Element("Region") == null)
							{
								AddTextureRegion(textureName.Value, new TextureRegion(content, new Rectangle(0, 0, content.Width, content.Height)));
							}
							else
							{
								foreach (var region in texture.Descendants("Region"))
								{
									var regionName = region.Attribute("name");
									if (regionName != null && !string.IsNullOrEmpty(regionName.Value))
									{
										if (region.Attribute("lefttop") != null)
										{
											var lefttop = region.Attribute("lefttop").Value.ToPoint();
											if (region.Attribute("rightbottom") != null ||
												(region.Attribute("width") != null && region.Attribute("height") != null))
											{
												Rectangle firstRegion;
												if (region.Attribute("rightbottom") != null)
												{
													var rightbottom = region.Attribute("rightbottom").Value.ToPoint();
													firstRegion = new Rectangle(lefttop.X, lefttop.Y, rightbottom.X - lefttop.X, rightbottom.Y - lefttop.Y);
												}
												else
												{
													var width = region.Attribute("width").Value.ToInt32();
													var height = region.Attribute("height").Value.ToInt32();
													firstRegion = new Rectangle(lefttop.X, lefttop.Y, width, height);
												}

												if (region.Attribute("totalFrames") != null)
												{
													var totalFrames = region.Attribute("totalFrames").Value.ToInt32();

													if (totalFrames > 1)
													{
														if (region.Attribute("direction") != null)
														{
															var direction = region.Attribute("direction").Value;
															if (IsValidDirection(direction) &&
																region.Attribute("maxFramesInLine") != null &&
																region.Attribute("frameStep") != null &&
																region.Attribute("lineStep") != null)
															{
																var maxFramesInLine = region.Attribute("maxFramesInLine").Value.ToInt32();
																if (totalFrames >= maxFramesInLine)
																{
																	var frameStep = region.Attribute("frameStep").Value.ToInt32();
																	var lineStep = region.Attribute("lineStep").Value.ToInt32();
																	var isHorizontal = direction.Equals("horizontal") ? true : false;

																	AddTextureRegion(regionName.Value,
																			new TextureRegion(content, firstRegion, totalFrames, maxFramesInLine, isHorizontal, frameStep, lineStep));
																}
															}
														}
														else if (region.Attribute("frameStep") != null)
														{
															var frameStep = region.Attribute("frameStep").Value.ToPoint();

															AddTextureRegion(regionName.Value,
																new TextureRegion(content, firstRegion, totalFrames, frameStep));
														}
													}
													else if (totalFrames > 0)
													{
														AddTextureRegion(regionName.Value, new TextureRegion(content, firstRegion));
													}
												}
												else
												{
													AddTextureRegion(regionName.Value, new TextureRegion(content, firstRegion));
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			else
			{
				throw new System.ArgumentNullException("doc");
			}
		}
	}
}