﻿namespace GmCore.Actions
{
	using Microsoft.Xna.Framework;

	public abstract class Action
	{
		private bool running;
		protected Sprite[] sprites;

		protected Action(params Sprite[] sprites)
		{
			this.sprites = sprites;
			this.running = true;
			this.ElapsedTime = 0f;
			this.Count = 0;
		}

		protected int Count { get; set; }

		protected float ElapsedTime { get; set; }

		public bool Running
		{
			get
			{
				return this.running;
			}
		}

		public abstract bool Finished
		{
			get;
		}

		public virtual void Initialize(params Sprite[] sprites)
		{
			if (sprites != null && sprites.Length > 0)
			{
				if (this.sprites == null || this.sprites.Length == 0 || this.sprites != sprites)
				{
					this.sprites = sprites;
				}
			}

			this.running = true;
			this.ElapsedTime = 0f;
			this.Count = 0;
		}

		public abstract void Update();

		public abstract void Update(GameTime gameTime);

		public void Stop()
		{
			this.running = false;
		}

		public void Start()
		{
			this.running = true;
		}

		protected virtual void DoFinalWork()
		{
			this.running = false;
		}
	}
}