﻿namespace GmCore.Actions
{
	using GmCore.Tweenings;
	using Microsoft.Xna.Framework;

	public class TranslationAction : SimultaneousAction
	{
		public TranslationAction(Vector2 from, Vector2 to, int frames, Tweening tweening, params Sprite[] sprites)
			: this(from, to, frames, tweening, tweening, sprites)
		{
		}

		public TranslationAction(Vector2 from, Vector2 to, int frames, Tweening xTweening, Tweening yTweening, params Sprite[] sprites)
			: base(
			new Action[]
			{
				new XTranslationAction(from.X, to.X, frames, xTweening),
				new YTranslationAction(from.Y, to.Y, frames, yTweening)
			},
			sprites)
		{
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <param name="duration">Seconds</param>
		/// <param name="tweening"></param>
		/// <param name="sprites"></param>
		public TranslationAction(Vector2 from, Vector2 to, float duration, Tweening tweening, params Sprite[] sprites)
			: this(from, to, duration, tweening, tweening, sprites)
		{
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <param name="duration">Seconds</param>
		/// <param name="xTweening"></param>
		/// <param name="yTweening"></param>
		/// <param name="sprites"></param>
		public TranslationAction(Vector2 from, Vector2 to, float duration, Tweening xTweening, Tweening yTweening, params Sprite[] sprites)
			: base(
			new Action[]
			{
				new XTranslationAction(from.X, to.X, duration, xTweening),
				new YTranslationAction(from.Y, to.Y, duration, yTweening)
			},
			sprites)
		{
		}
	}
}