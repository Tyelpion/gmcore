﻿namespace GmCore.Actions
{
	using Microsoft.Xna.Framework;
	using System.Collections.Generic;

	public class SimultaneousAction : Action
	{
		private Deque<Action> actions;

		public SimultaneousAction(IList<Action> actions, params Sprite[] sprites)
			: base(sprites)
		{
			if (actions != null)
			{
				this.actions = new Deque<Action>(actions);
			}
			else
			{
				throw new System.ArgumentNullException("actions");
			}
		}

		public override bool Finished
		{
			get
			{
				foreach (var action in this.actions)
				{
					if (!action.Finished)
					{
						return false;
					}
				}

				return true;
			}
		}

		public override void Initialize(params Sprite[] sprites)
		{
			base.Initialize(sprites);
			foreach (var action in this.actions)
			{
				action.Initialize(this.sprites);
			}
		}

		public override void Update()
		{
			if (this.Running)
			{
				foreach (var action in this.actions)
				{
					if (!action.Finished)
					{
						action.Update();
					}
				}
			}
		}

		public override void Update(GameTime gameTime)
		{
			if (this.Running)
			{
				foreach (var action in this.actions)
				{
					if (!action.Finished)
					{
						action.Update(gameTime);
					}
				}
			}
		}

		public void AddAction(Action action)
		{
			if (!this.actions.Contains(action))
			{
				this.actions.AddBack(action);
			}
		}

		public void RemoveAction(Action action)
		{
			if (this.actions.Contains(action))
			{
				this.actions.Remove(action);
			}
		}
	}
}