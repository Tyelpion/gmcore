﻿namespace GmCore.Actions
{
	using Microsoft.Xna.Framework;

	public class DelayAction : Action
	{
		private Action action;
		private float duration;
		private int frames;

		public DelayAction(Action action, int frames, params Sprite[] sprites)
			: base(sprites)
		{
			this.action = action;
			this.frames = frames;
			this.duration = -1f;
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="action"></param>
		/// <param name="duration">Seconds</param>
		/// <param name="sprites"></param>
		public DelayAction(Action action, float duration, params Sprite[] sprites)
			: base(sprites)
		{
			this.action = action;
			this.duration = duration;
			this.frames = -1;
		}

		public override bool Finished
		{
			get { return (this.ElapsedTime >= this.duration || this.Count == this.frames) && this.action.Finished; }
		}

		public override void Update()
		{
			if (this.Running && this.frames >= 0)
			{
				if (this.Count < this.frames)
				{
					this.Count += 1;

					if (this.Count == this.frames)
					{
						this.action.Initialize(this.sprites);
					}
					return;
				}

				this.action.Update();
			}
		}

		public override void Update(GameTime gameTime)
		{
			if (this.Running && this.duration >= 0f)
			{
				if (this.ElapsedTime < this.duration)
				{
					this.ElapsedTime += (float) gameTime.ElapsedGameTime.TotalSeconds;

					if (this.ElapsedTime >= this.duration)
					{
						this.action.Initialize(this.sprites);
					}
					return;
				}

				this.action.Update(gameTime);
			}
		}
	}
}