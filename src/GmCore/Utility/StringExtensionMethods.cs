﻿namespace GmCore
{
	using Microsoft.Xna.Framework;

	public static class StringExtensionMethods
	{
		public static Vector2 ToVector2(this string value)
		{
			if (!string.IsNullOrEmpty(value))
			{
				var arr = value.Split();
				if (arr.Length == 2)
				{
					float x, y;
					if (float.TryParse(arr[0], out x) && float.TryParse(arr[1], out y))
					{
						return new Vector2(x, y);
					}
				}
			}

			return Vector2.Zero;
		}

		public static Point ToPoint(this string value)
		{
			if (!string.IsNullOrEmpty(value))
			{
				var arr = value.Split();
				if (arr.Length == 2)
				{
					int x, y;
					if (int.TryParse(arr[0], out x) && int.TryParse(arr[1], out y))
					{
						return new Point(x, y);
					}
				}

				return Point.Zero;
			}

			return Point.Zero;
		}

		public static float ToFloat(this string value)
		{
			if (!string.IsNullOrEmpty(value))
			{
				float val;
				if (float.TryParse(value, out val))
				{
					return val;
				}
			}

			return 0;
		}

		public static int ToInt32(this string value)
		{
			if (!string.IsNullOrEmpty(value))
			{
				int val;
				if (int.TryParse(value, out val))
				{
					return val;
				}
			}

			return 0;
		}
	}
}