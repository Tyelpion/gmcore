﻿namespace GmCore
{
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Graphics;

	public static class TextureExtensionMethods
	{
		public static Color[] ToArray(this Texture2D texture)
		{
			return texture.ToArray(new Rectangle(0, 0, texture.Width, texture.Height));
		}

		public static Color[] ToArray(this Texture2D texture, Rectangle region)
		{
			var colors = new Color[region.Width * region.Height];
			texture.GetData(0, region, colors, 0, colors.Length);
			return colors;
		}

		public static Color[,] To2DArray(this Texture2D texture)
		{
			return texture.To2DArray(new Rectangle(0, 0, texture.Width, texture.Height));
		}

		public static Color[,] To2DArray(this Texture2D texture, Rectangle region)
		{
			var colors1D = texture.ToArray(region);
			var colors2D = new Color[region.Width, region.Height];

			for (var x = 0; x < region.Width; ++x)
			{
				for (var y = 0; y < region.Height; ++y)
				{
					colors2D[x, y] = colors1D[x + y * region.Width];
				}
			}

			return colors2D;
		}
	}
}