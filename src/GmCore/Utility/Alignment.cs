﻿namespace GmCore
{
	[System.Flags]
	public enum Alignment
	{
		HorizontalLeft = 0x01,
		HorizontalRight = 0x02,
		HorizontalCenter = 0x03,

		VerticalTop = 0x10,
		VerticalBottom = 0x20,
		VerticalMiddle = 0x30,

		TopLeft = 0x11,
		TopRight = 0x12,
		TopCenter = 0x13,

		BottomLeft = 0x21,
		BottomRight = 0x22,
		BottomCenter = 0x23,

		MiddleLeft = 0x31,
		MiddleRight = 0x32,
		MiddleCenter = 0x33,
	}
}