﻿namespace GmCore
{
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Graphics;
	using System.Text;

	public class FontSprite : Sprite
	{
		private SpriteFont font;
		private StringBuilder text;
		private bool begin;

		public FontSprite(Vector2 position, SpriteFont font)
			: this(position, Alignment.TopLeft, font)
		{
		}

		public FontSprite(Vector2 position, Alignment originPosition, SpriteFont font)
			: this(position, originPosition, font, Color.White)
		{
		}

		public FontSprite(Vector2 position, Alignment originPosition, SpriteFont font, Color color)
			: this(position, originPosition, font, 0f, color, 1f)
		{
		}

		public FontSprite(Vector2 position, Alignment originPosition, SpriteFont font, float angle, Color color, float scale)
			: base(position, originPosition, angle, color, scale)
		{
			this.font = font;
			this.text = new StringBuilder();
			this.begin = false;
		}

		public SpriteFont Font
		{
			get { return this.font; }
		}

		public string Text
		{
			get
			{
				return this.text.ToString();
			}

			set
			{
				this.UpdateText(value);
			}
		}

		public void UpdateText(string value)
		{
			this.text.Clear();
			this.text.Append(value);
			var size = this.font.MeasureString(value);
			this.Boundary = new Rectangle(0, 0, (int) size.X, (int) size.Y);
		}

		public void Begin()
		{
			this.begin = true;
		}

		public void Begin(string text)
		{
			this.UpdateText(text);
			this.Begin();
		}

		public void End()
		{
			this.begin = false;
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			if (this.begin)
			{
				spriteBatch.Begin();
				spriteBatch.DrawString(this.font, this.text, this.Position, this.Color, this.Angle, this.Origin, this.Scale, SpriteEffects.None, 1f);
				spriteBatch.End();
			}

			base.Draw(spriteBatch);
		}
	}
}