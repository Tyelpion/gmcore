﻿namespace GmCore
{
	using System.Text;
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Graphics;

	public class TextImageButton : ImageButton
	{
		private StringBuilder text;
		private Color textColor;
		private Vector2 textPosition;
		private Vector2 textOrigin;
		private Vector2 textAdjust;
		private Vector2 textSize;
		private Alignment textAlignment;
		private SpriteFont font;

		public TextImageButton(Screen parent, Vector2 position, SpriteFont font, TextureRegion upImage, TextureRegion downImage = null, TextureRegion disabledImage = null)
			: this(parent, position, Alignment.TopLeft, font, upImage, downImage, disabledImage)
		{
		}

		public TextImageButton(Screen parent, Vector2 position, Alignment originPosition, SpriteFont font, TextureRegion upImage, TextureRegion downImage = null, TextureRegion disabledImage = null)
			: this(parent, position, originPosition, font, upImage, downImage, disabledImage, Color.White)
		{
		}

		public TextImageButton(Screen parent, Vector2 position, Alignment originPosition, SpriteFont font, TextureRegion upImage, TextureRegion downImage, TextureRegion disabledImage, Color color)
			: base(parent, position, originPosition, upImage, downImage, disabledImage, color)
		{
			this.font = font;
			this.text = new StringBuilder();
		}

		public string Text
		{
			get { return this.text.ToString(); }
		}

		public void SetText(string text)
		{
			this.SetText(text, Color.White);
		}

		public void SetText(string text, Vector2 adjust)
		{
			this.SetText(text, Color.White, Alignment.TopLeft, adjust);
		}

		public void SetText(string text, Color textColor)
		{
			this.SetText(text, textColor, Alignment.TopLeft);
		}

		public void SetText(string text, Color textColor, Vector2 adjust)
		{
			this.SetText(text, textColor, Alignment.TopLeft, adjust);
		}

		public void SetText(string text, Alignment textAlignment)
		{
			this.SetText(text, Color.White, textAlignment);
		}

		public void SetText(string text, Alignment textAlignment, Vector2 adjust)
		{
			this.SetText(text, Color.White, textAlignment, adjust);
		}

		public void SetText(string text, Color textColor, Alignment textAlignment)
		{
			this.SetText(text, textColor, textAlignment, Vector2.Zero);
		}

		public void SetText(string text, Color textColor, Alignment textAlignment, Vector2 adjust)
		{
			this.text.Clear();
			this.text.Append(text);
			this.textColor = textColor;
			this.textAdjust = adjust;
			this.textSize = this.font.MeasureString(this.text);
			this.ResetAlignment(textAlignment);
		}

		public void ResetAlignment(Alignment alignment)
		{
			this.textAlignment = alignment;

			switch ((Alignment) ((int) this.textAlignment & 0x0f))
			{
				case Alignment.HorizontalLeft:
					this.textOrigin.X = 0f;
					break;

				case Alignment.HorizontalCenter:
					this.textOrigin.X = this.textSize.X / 2f;
					this.textPosition.X = this.Width / 2f;
					break;

				case Alignment.HorizontalRight:
					this.textOrigin.X = this.textSize.X;
					this.textPosition.X = this.Width;
					break;

				default:
					break;
			}

			switch ((Alignment) ((int) this.textAlignment & 0xf0))
			{
				case Alignment.VerticalTop:
					this.textOrigin.Y = 0f;
					break;

				case Alignment.VerticalMiddle:
					this.textOrigin.Y = this.textSize.Y / 2f;
					this.textPosition.Y = this.Height / 2f;
					break;

				case Alignment.VerticalBottom:
					this.textOrigin.Y = this.textSize.Y;
					this.textPosition.Y = this.Height;
					break;

				default:
					break;
			}

			this.textPosition += this.Offset + this.textAdjust;
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			base.Draw(spriteBatch);
			spriteBatch.Begin();
			spriteBatch.DrawString(this.font, this.text, this.textPosition, this.textColor, this.Angle, this.textOrigin, this.Scale, SpriteEffects.None, 1f);
			spriteBatch.End();
		}
	}
}