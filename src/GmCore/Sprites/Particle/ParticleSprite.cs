﻿namespace GmCore
{
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Graphics;
	using System;
	using System.Collections.Generic;

	public class ParticleSprite : Sprite
	{
		public Vector2 ParticleOffset;

		private List<ParticleEmitter> emitters;
		private Vector2 lastPosition;
		private Random random;

		public ParticleSprite(Vector2 position)
			: this(position, Vector2.Zero)
		{
		}

		public ParticleSprite(Vector2 position, Vector2 particleOffset)
			: base(position, Alignment.MiddleCenter)
		{
			this.lastPosition = this.Position;
			this.random = new Random();
			this.emitters = new List<ParticleEmitter>();
			this.ParticleOffset = particleOffset;
		}

		public bool Finished
		{
			get;
			private set;
		}

		public Vector2 LastPosition
		{
			get { return this.lastPosition; }
		}

		public List<ParticleEmitter> Emitters
		{
			get { return this.emitters; }
		}

		public void Clear()
		{
			for (int i = 0; i < this.emitters.Count; i++)
			{
				if (this.emitters[i].MaxAliveParticles > 0)
				{
					this.emitters[i].Clear();
				}
			}
		}

		public void AddEmitter(ParticleEmitter emitter)
		{
			if (!this.emitters.Contains(emitter))
			{
				this.emitters.Add(emitter);
			}
		}

		public void AddEmitter(Vector2 timePerSpawn, Vector2 spawnDirection, Vector2 spawnNoiseAngle,
							   Vector2 startLife, Vector2 startScale, Vector2 endScale,
							   Color startColor1, Color endColor1, Color startColor2, Color endColor2,
							   Vector2 startSpeed, Vector2 endSpeed, int maxAliveParticles, Vector2 relativePosition,
							   TextureRegion textureRegion)
		{
			this.AddEmitter(timePerSpawn, 1000f, float.NaN, spawnDirection, spawnNoiseAngle,
							startLife, startScale, endScale,
							startColor1, endColor1, startColor2, endColor2,
							startSpeed, endSpeed, maxAliveParticles, relativePosition, textureRegion);
		}

		public void AddEmitter(Vector2 timePerSpawn, float timeScale, float duration, Vector2 spawnDirection, Vector2 spawnNoiseAngle,
							   Vector2 startLife, Vector2 startScale, Vector2 endScale,
							   Color startColor1, Color endColor1, Color startColor2, Color endColor2,
							   Vector2 startSpeed, Vector2 endSpeed, int maxAliveParticles, Vector2 relativePosition,
							   TextureRegion textureRegion)
		{
			this.emitters.Add(new ParticleEmitter(this,
													timePerSpawn, timeScale, duration, spawnDirection, spawnNoiseAngle,
													startLife, startScale, endScale,
													startColor1, endColor1, startColor2, endColor2,
													startSpeed, endSpeed,
													maxAliveParticles, relativePosition,
													this.random, textureRegion));
		}

		public override void Update(GameTime gameTime)
		{
			if (!this.Finished)
			{
				var finished = true;
				for (int i = 0; i < this.emitters.Count; i++)
				{
					if (this.emitters[i].MaxAliveParticles > 0)
					{
						this.emitters[i].Update(gameTime);
						finished = false;
					}
				}
				this.Finished = finished;
			}
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			if (!this.Finished)
			{
				spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Additive);
				for (int i = 0; i < this.emitters.Count; i++)
				{
					if (this.emitters[i].MaxAliveParticles > 0)
					{
						this.emitters[i].Draw(spriteBatch);
					}
				}
				spriteBatch.End();
			}
		}

		public override void GoTo(Vector2 position)
		{
			this.lastPosition = this.Position;
			base.GoTo(position);
		}

		public void Reset()
		{
			for (int i = 0; i < this.emitters.Count; i++)
			{
				this.emitters[i].Reset();
			}
			this.Finished = false;
		}
	}
}