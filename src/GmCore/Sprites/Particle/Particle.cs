﻿namespace GmCore
{
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Graphics;

	public class Particle
	{
		public Vector2 Position;

		private ParticleEmitter parent;
		private Vector2 startDirection, endDirection;
		private float startLife, lifeLeft, lifePhase;
		private float startScale, endScale;
		private Color startColor, endColor;

		public Particle(ParticleEmitter parent, Vector2 position, Vector2 startDirection, Vector2 endDirection, float startLife, float startScale, float endScale, Color startColor, Color endColor)
		{
			this.parent = parent;
			this.Position = position;
			this.startDirection = startDirection;
			this.endDirection = endDirection;
			this.startLife = startLife;
			this.lifeLeft = startLife;
			this.startScale = startScale;
			this.endScale = endScale;
			this.startColor = startColor;
			this.endColor = endColor;
		}

		public bool Update(float dt)
		{
			this.lifeLeft -= dt;
			if (this.lifeLeft < 0)
				return false;

			// lifePhase = 1 : new
			// lifePhase = 0 : dead
			this.lifePhase = this.lifeLeft / this.startLife;
			this.Position += Vector2.Lerp(this.endDirection, this.startDirection, this.lifePhase) * dt;
			return true;
		}

		public void Draw(SpriteBatch spriteBatch, float scale, Vector2 offset)
		{
			var thisScale = MathHelper.Lerp(this.endScale, this.startScale, this.lifePhase);
			var color = Color.Lerp(this.endColor, this.startColor, this.lifePhase);
			var rectangle = new Rectangle((int) ((this.Position.X - 0.5f * thisScale) * scale + offset.X),
										  (int) ((this.Position.Y - 0.5f * thisScale) * scale + offset.Y),
										  (int) (thisScale * scale), (int) (thisScale * scale));

			spriteBatch.Draw(this.parent.TextureRegion.Texture, rectangle, this.parent.TextureRegion.Region, color, 0f, Vector2.Zero, SpriteEffects.None, 0);
		}
	}
}