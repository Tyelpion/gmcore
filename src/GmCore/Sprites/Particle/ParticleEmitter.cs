﻿namespace GmCore
{
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Graphics;
	using System;
	using System.Collections.Generic;

	public class ParticleEmitter
	{
		public Vector2 RelativePosition;
		public int MaxAliveParticles;
		public Vector2 TimePerSpawn;
		public float TimeScale;
		public float Duration;
		public Vector2 SpawnDirection;
		public Vector2 SpawnNoiseAngle;
		public Vector2 StartLife;
		public Vector2 StartScale;
		public Vector2 EndScale;
		public Color StartColor1;
		public Color StartColor2;
		public Color EndColor1;
		public Color EndColor2;
		public Vector2 StartSpeed;
		public Vector2 EndSpeed;

		private ParticleSprite parent;
		private Random random;
		private TextureRegion textureRegion;
		private LinkedList<Particle> activePartices;
		private float nextSpawnTime;
		private float deltaTime;
		private float elapsedTime;
		private bool timeout;

		public ParticleEmitter(ParticleSprite parent,
							   Vector2 timePerSpawn, Vector2 spawnDirection, Vector2 spawnNoiseAngle,
							   Vector2 startLife, Vector2 startScale, Vector2 endScale,
							   Color startColor1, Color endColor1, Color startColor2, Color endColor2,
							   Vector2 startSpeed, Vector2 endSpeed, int maxAliveParticles, Vector2 relativePosition,
							   Random random, TextureRegion textureRegion)
			: this(parent,
					timePerSpawn, 1000f, float.NaN, spawnDirection, spawnNoiseAngle,
					startLife, startScale, endScale,
					startColor1, endColor1, startColor2, endColor2,
					startSpeed, endSpeed, maxAliveParticles, relativePosition,
					random, textureRegion)
		{
		}

		public ParticleEmitter(ParticleSprite parent,
							   Vector2 timePerSpawn, float timeScale, float duration, Vector2 spawnDirection, Vector2 spawnNoiseAngle,
							   Vector2 startLife, Vector2 startScale, Vector2 endScale,
							   Color startColor1, Color endColor1, Color startColor2, Color endColor2,
							   Vector2 startSpeed, Vector2 endSpeed, int maxAliveParticles, Vector2 relativePosition,
							   Random random, TextureRegion textureRegion)
		{
			this.parent = parent;
			this.TimePerSpawn = timePerSpawn;
			this.TimeScale = timeScale;
			this.Duration = duration;
			this.SpawnDirection = spawnDirection;
			this.SpawnNoiseAngle = spawnNoiseAngle;
			this.StartLife = startLife;
			this.StartScale = startScale;
			this.EndScale = endScale;
			this.StartColor1 = startColor1;
			this.EndColor1 = endColor1;
			this.StartColor2 = startColor2;
			this.EndColor2 = endColor2;
			this.StartSpeed = startSpeed;
			this.EndSpeed = endSpeed;
			this.MaxAliveParticles = maxAliveParticles;
			this.RelativePosition = relativePosition;
			this.textureRegion = textureRegion;
			this.random = random;
			this.activePartices = new LinkedList<Particle>();
			this.Reset();
		}

		public TextureRegion TextureRegion
		{
			get { return this.textureRegion; }
		}

		public void Clear()
		{
			this.activePartices.Clear();
		}

		public void Reset()
		{
			this.Clear();
			this.nextSpawnTime = MathHelper.Lerp(this.TimePerSpawn.Y, this.TimePerSpawn.X, (float) this.random.NextDouble());
			this.deltaTime = 0f;
			this.elapsedTime = 0f;
			this.timeout = false;
		}

		public void Reset(Vector2 timePerSpawn)
		{
			this.TimePerSpawn = timePerSpawn;
			this.Reset();
		}

		public void Update(GameTime gameTime)
		{
			if (float.IsNaN(this.TimeScale) || float.IsInfinity(this.TimeScale))
				return;

			if (!this.timeout && !float.IsNaN(this.Duration) && !float.IsInfinity(this.Duration))
			{
				this.elapsedTime += gameTime.ElapsedGameTime.Milliseconds;

				if (this.elapsedTime > this.Duration)
				{
					this.TimePerSpawn = new Vector2(float.NaN, float.NaN);
					this.timeout = true;
				}
			}

			var dt = gameTime.ElapsedGameTime.Milliseconds / this.TimeScale;
			this.deltaTime += dt;
			while (this.deltaTime > this.nextSpawnTime)
			{
				if (this.activePartices.Count < this.MaxAliveParticles)
				{
					var position = this.RelativePosition + Vector2.Lerp(this.parent.LastPosition, this.parent.Position, this.deltaTime / dt);
					var radians = MathHelper.Lerp(this.SpawnNoiseAngle.X, this.SpawnNoiseAngle.Y, (float) this.random.NextDouble());
					var startDirection = Vector2.Transform(this.SpawnDirection, Matrix.CreateRotationZ(radians));
					startDirection.Normalize();
					var endDirection = startDirection * MathHelper.Lerp(this.EndSpeed.X, this.EndSpeed.Y, (float) random.NextDouble());
					startDirection *= MathHelper.Lerp(this.StartSpeed.X, this.StartSpeed.Y, (float) this.random.NextDouble());
					var startLife = MathHelper.Lerp(this.StartLife.X, this.StartLife.Y, (float) this.random.NextDouble());
					var startScale = MathHelper.Lerp(this.StartScale.X, this.StartScale.Y, (float) this.random.NextDouble());
					var endScale = MathHelper.Lerp(this.EndScale.X, this.EndScale.Y, (float) this.random.NextDouble());
					var startColor = Color.Lerp(this.StartColor1, this.StartColor2, (float) this.random.NextDouble());
					var endColor = Color.Lerp(this.EndColor1, this.EndColor2, (float) this.random.NextDouble());

					this.activePartices.AddLast(new Particle(this, position, startDirection, endDirection, startLife, startScale, endScale, startColor, endColor));
					this.activePartices.Last.Value.Update(this.deltaTime);
				}
				this.deltaTime -= this.nextSpawnTime;
				this.nextSpawnTime = MathHelper.Lerp(this.TimePerSpawn.X, this.TimePerSpawn.Y, (float) this.random.NextDouble());
			}

			var node = this.activePartices.First;
			while (node != null)
			{
				var alive = node.Value.Update(dt);
				node = node.Next;
				if (!alive)
				{
					if (node == null)
					{
						this.activePartices.RemoveLast();
					}
					else
					{
						this.activePartices.Remove(node.Previous);
					}
				}
			}
		}

		public void Draw(SpriteBatch spriteBatch)
		{
			var node = this.activePartices.First;
			while (node != null)
			{
				node.Value.Draw(spriteBatch, this.parent.Scale, this.parent.ParticleOffset);
				node = node.Next;
			}
		}
	}
}