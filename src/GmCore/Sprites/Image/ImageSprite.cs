﻿namespace GmCore
{
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Graphics;
	using System;

	public class ImageSprite : Sprite
	{
		protected TextureRegion textureRegion;
		protected int pixelAlpha;

		public ImageSprite(TextureRegion textureRegion)
			: this(Vector2.Zero, Alignment.TopLeft, textureRegion)
		{
		}

		public ImageSprite(Vector2 position, TextureRegion textureRegion)
			: this(position, Alignment.TopLeft, textureRegion, Color.White)
		{
		}

		public ImageSprite(Vector2 position, Alignment originPosition, TextureRegion textureRegion)
			: this(position, originPosition, textureRegion, Color.White)
		{
		}

		public ImageSprite(Vector2 position, Alignment originPosition, TextureRegion textureRegion, Color color)
			: this(position, originPosition, textureRegion, 0f, color)
		{
		}

		public ImageSprite(Vector2 position, Alignment originPosition, TextureRegion textureRegion, float angle, Color color)
			: this(position, originPosition, textureRegion, angle, color, 1f)
		{
		}

		public ImageSprite(Vector2 position, Alignment originPosition, TextureRegion textureRegion, float angle, Color color, float scale)
			: base(position, originPosition, angle, color, scale)
		{
			this.ChangeTextureRegion(textureRegion);
		}

		public TextureRegion TextureRegion
		{
			get { return this.textureRegion; }
		}

		public override int Width
		{
			get { return this.textureRegion.Width; }
		}

		public override int Height
		{
			get { return this.textureRegion.Height; }
		}

		public Color[,] Data
		{
			get { return this.textureRegion.Data; }
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			spriteBatch.Begin();
			spriteBatch.Draw(this.textureRegion.Texture, this.Position, this.textureRegion.Region, this.Color, this.Angle, this.Origin, this.Scale, SpriteEffects.None, 0f);
			spriteBatch.End();

			base.Draw(spriteBatch);
		}

		public void ChangeTextureRegion(TextureRegion textureRegion)
		{
			if (textureRegion != null)
			{
				this.textureRegion = textureRegion;
				this.SetOrigin(this.textureRegion.Region);
				this.SetBoundary(this.textureRegion.Region);
				this.pixelAlpha = 0;
			}
			else
			{
				throw new System.ArgumentNullException("textureRegion");
			}
		}

		protected override void SetBoundary(int width, int height)
		{
			var transform = this.Transform;

			var lt = Vector2.Transform(Vector2.Zero, transform);
			var rt = Vector2.Transform(new Vector2(width, 0), transform);
			var lb = Vector2.Transform(new Vector2(0, height), transform);
			var rb = Vector2.Transform(new Vector2(width, height), transform);

			var min = Vector2.Min(Vector2.Min(lt, rt), Vector2.Min(lb, rb));
			var max = Vector2.Max(Vector2.Max(lt, rt), Vector2.Max(lb, rb));

			this.Boundary = new Rectangle((int) min.X, (int) min.Y, (int) (max.X - min.X), (int) (max.Y - min.Y));
		}

		public virtual bool PixelIntersects(ImageSprite sprite)
		{
			var transform = this.Transform * Matrix.Invert(sprite.Transform);

			var stepX = Vector2.TransformNormal(Vector2.UnitX, transform);
			var stepY = Vector2.TransformNormal(Vector2.UnitY, transform);
			var yPosInB = Vector2.Transform(Vector2.Zero, transform);

			for (var yA = 0; yA < this.Height; ++yA)
			{
				var posInB = yPosInB;
				for (var xA = 0; xA < this.Width; ++xA)
				{
					var xB = (int) Math.Round(posInB.X);
					var yB = (int) Math.Round(posInB.Y);

					if (xB >= 0 && xB < sprite.Width && yB >= 0 && yB < sprite.Height)
					{
						var colorA = this.Data[xA, yA];
						var colorB = sprite.Data[xB, yB];

						if (colorA.A > this.pixelAlpha && colorB.A > sprite.pixelAlpha)
							return true;
					}
					posInB += stepX;
				}
				yPosInB += stepY;
			}
			return false;
		}
	}
}