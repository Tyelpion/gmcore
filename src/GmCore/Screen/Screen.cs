﻿namespace GmCore
{
	using Microsoft.Xna.Framework;
	using Microsoft.Xna.Framework.Graphics;
	using Microsoft.Xna.Framework.Input;
	using System.Collections.Generic;

	public abstract class Screen
	{
		private Sprite root;
		private List<IButton> buttons;
		private Rectangle boundary;

		protected Screen()
			: this(0, 0)
		{
		}

		protected Screen(int width, int height)
		{
			this.root = new Sprite();
			this.buttons = new List<IButton>();
			this.boundary = new Rectangle(0, 0, width, height);
		}

		public Sprite Root
		{
			get
			{
				return this.root;
			}
		}

		public int ScreenWidth
		{
			get
			{
				return this.boundary.Width;
			}
		}

		public int ScreenHeight
		{
			get
			{
				return this.boundary.Height;
			}
		}

		public abstract void Start();

		public abstract void End();

		public abstract void UpdateLogic(GameTime gameTime);

		public bool Contains(int x, int y)
		{
			return this.boundary.Contains(x, y);
		}

		public bool Contains(Point value)
		{
			return this.boundary.Contains(value);
		}

		public bool Contains(Rectangle value)
		{
			return this.boundary.Contains(value);
		}

		public bool Contains(MouseState mouse)
		{
			return this.boundary.Contains(mouse.X, mouse.Y);
		}

		public void Update(GameTime gameTime)
		{
			this.UpdateButton();
			this.UpdateLogic(gameTime);
		}

		public void AddChild(Sprite child)
		{
			this.root.AddChild(child);
		}

		public void AddChild(params Sprite[] children)
		{
			this.root.AddChild(children);
		}

		public void AddChild(IEnumerable<Sprite> children1, params Sprite[] children2)
		{
			this.root.AddChild(children1, children2);
		}

		public void AddButton(IButton button)
		{
			if (button == null)
				throw new System.ArgumentNullException("button");

			if (!this.buttons.Contains(button))
			{
				this.buttons.Add(button);
			}
		}

		public void AddButton(params IButton[] buttons)
		{
			if (buttons == null)
				throw new System.ArgumentNullException("buttons");

			foreach (var button in buttons)
			{
				this.AddButton(button);
			}
		}

		public void AddButton(IEnumerable<IButton> buttons1, params IButton[] buttons2)
		{
			this.AddButton(buttons1);

			if (buttons2 != null)
			{
				this.AddButton(buttons2);
			}
		}

		public void RemoveChild(Sprite child)
		{
			this.root.RemoveChild(child);
		}

		public void RemoveChild(params Sprite[] children)
		{
			this.root.RemoveChild(children);
		}

		public void RemoveChild(IEnumerable<Sprite> children1, params Sprite[] children2)
		{
			this.root.RemoveChild(children1, children2);
		}

		public void RemoveButton(IButton button)
		{
			if (button == null)
				throw new System.ArgumentNullException("button");

			if (!this.buttons.Contains(button))
			{
				this.buttons.Remove(button);
			}
		}

		public void RemoveButton(params IButton[] buttons)
		{
			if (buttons == null)
				throw new System.ArgumentNullException("buttons");

			foreach (var button in buttons)
			{
				this.RemoveButton(button);
			}
		}

		public void RemoveButton(IEnumerable<IButton> buttons1, params IButton[] buttons2)
		{
			this.RemoveButton(buttons1);

			if (buttons2 != null)
			{
				this.RemoveButton(buttons2);
			}
		}

		public void Draw(SpriteBatch spriteBatch)
		{
			this.root.Draw(spriteBatch);
		}

		private void UpdateButton()
		{
			for (var i = 0; i < buttons.Count; ++i)
			{
				this.buttons[i].UpdateVisualState();
			}
		}
	}
}